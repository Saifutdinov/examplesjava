package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.mybatis.dao.SubjectDao;

import java.util.List;

public class SubjectDaoImpl extends DaoImplBase implements SubjectDao {
    @Override
    public Subject insert(Subject subject) {
        return null;
    }

    @Override
    public Subject getById(int id) {
        return null;
    }

    @Override
    public List<Subject> getAll() {
        return null;
    }

    @Override
    public Subject update(Subject subject) {
        return null;
    }

    @Override
    public void delete(Subject subject) {

    }

    @Override
    public void deleteAll() {

    }
}
