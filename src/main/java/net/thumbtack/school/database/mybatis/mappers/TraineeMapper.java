package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Trainee;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface TraineeMapper {

    @Insert("INSERT INTO trainee (firstName,lastName,rating) VALUES(#{firstName},#{lastName},#{rating})")
    @Options(useGeneratedKeys = true)
    Integer insert(Trainee trainee);

    @Insert("INSERT INTO trainee (firstName,lastName,rating,group_id) VALUES(#{trainee.firstName},#{trainee.lastName}," +
            "#{trainee.rating},#{groupId})")
    @Options(useGeneratedKeys = true)
    Integer insertWithGroup(@Param("trainee")Trainee trainee, @Param("groupId")int groupId);

    @Insert("INSERT INTO trainee (firstName,lastName,rating) VALUES(#{firstName},#{lastName},#{rating})")
    @Options(useGeneratedKeys = true)
    Integer insert(List<Trainee> trainees);



    @Select("SELECT id, firstName, lastName, rating FROM trainee WHERE id = #{id}")
    Trainee getById(int id);

    @Select({"<script>",
            "SELECT id, firstName, lastName, rating FROM trainee " +
            "<where>" +
            "<if test='#{firstName} != null'> fistName = #{firstName}</if>",
            "<if test='#{lastName} != null'> AND lastName = #{lastName}</if>",
            "<if test='#{rating} != null'> AND rating = #{rating}</if>",
            "</where>" +
                    "</script>" })
    List<Trainee> getByParams(@Param("firstName")String firstName,
                             @Param("lastName")String lastName,
                             @Param("rating")Integer rating);

    @Select("SELECT id, firstName, lastName, rating FROM trainee WHERE group_id = #{groupId}")
    List<Trainee> getByGroupId(int groupId);

    @Select("SELECT * FROM trainee")
    List<Trainee> getAll();


    @Update("UPDATE trainee SET firstName = #{firstName}, lastName = #{lastName}, rating = #{rating}, WHERE id = #{id}")
    void update(Trainee trainee);

    @Update("UPDATE trainee SET group_id = #{groupId} WHERE id = #{id}")
    void refToGroup(@Param("id")int traineeId, @Param("groupId")int groupId);

    @Update("UPDATE trainee SET group_id = null WHERE id = #{id}")
    void deleteRefToGroup(int id);

    @Delete("DELETE FROM trainee WHERE id = #{id}")
    void delete(int id);

    @Delete("DELETE FROM trainee")
    void deleteAll();
}
