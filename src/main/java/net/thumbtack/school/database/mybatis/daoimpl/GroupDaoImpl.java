package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;
import net.thumbtack.school.database.mybatis.dao.GroupDao;

import java.util.List;

public class GroupDaoImpl extends DaoImplBase implements GroupDao {
    @Override
    public Group insert(School school, Group group) {
        return null;
    }

    @Override
    public Group update(Group group) {
        return null;
    }

    @Override
    public List<Group> getAll() {
        return null;
    }

    @Override
    public void delete(Group group) {

    }

    @Override
    public Trainee moveTraineeToGroup(Group group, Trainee trainee) {
        return null;
    }

    @Override
    public void deleteTraineeFromGroup(Trainee trainee) {

    }

    @Override
    public void addSubjectToGroup(Group group, Subject subject) {

    }
}
