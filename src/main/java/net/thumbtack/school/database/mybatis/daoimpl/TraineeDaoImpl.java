package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.Trainee;
import net.thumbtack.school.database.mybatis.dao.TraineeDao;

import java.util.List;

public class TraineeDaoImpl extends DaoImplBase implements TraineeDao {
    @Override
    public Trainee insert(Group group, Trainee trainee) {
        return null;
    }

    @Override
    public Trainee getById(int id) {
        return null;
    }

    @Override
    public List<Trainee> getAll() {
        return null;
    }

    @Override
    public Trainee update(Trainee trainee) {
        return null;
    }

    @Override
    public List<Trainee> getAllWithParams(String firstName, String lastName, Integer rating) {
        return null;
    }

    @Override
    public void batchInsert(List<Trainee> trainees) {

    }

    @Override
    public void delete(Trainee trainee) {

    }

    @Override
    public void deleteAll() {

    }
}
