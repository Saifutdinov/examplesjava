package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.mybatis.dao.SchoolDao;

import java.util.List;

public class SchoolDaoImpl extends DaoImplBase implements SchoolDao {
    @Override
    public School insert(School school) {
        return null;
    }

    @Override
    public School getById(int id) {
        return null;
    }

    @Override
    public List<School> getAllLazy() {
        return null;
    }

    @Override
    public List<School> getAllUsingJoin() {
        return null;
    }

    @Override
    public void update(School school) {

    }

    @Override
    public void delete(School school) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public School insertSchoolTransactional(School school2018) {
        return null;
    }
}
